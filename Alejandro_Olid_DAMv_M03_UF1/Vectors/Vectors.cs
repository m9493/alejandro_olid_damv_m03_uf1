﻿// Autor: Alejandro Olid

using System;
using System.Collections;
using System.Linq;

namespace Vectors
{
    class vector
    {
        static void Main()
        {
            Menu();
        }

        static void Menu()
        {
            int n;
            do
            {
                Console.WriteLine("INDICE EJERCICIOS:");
                Console.WriteLine(" 01: DayOfTheWeek");
                Console.WriteLine(" 02: PlayerNumbers");
                Console.WriteLine(" 03: CandidateList");
                Console.WriteLine(" 04: LetterInWord");
                Console.WriteLine(" 05: AddValuesToList");
                Console.WriteLine(" 06: Swap");
                Console.WriteLine(" 07: PushButtonPadlockSimulator");
                Console.WriteLine(" 08: BoxesOpenedCounter");
                Console.WriteLine(" 09: MinOf10Values");
                Console.WriteLine(" 10: IsThereAMultipleOf7");
                Console.WriteLine(" 11: SearchInOrdered");
                Console.WriteLine(" 12: InverseOrder");
                Console.WriteLine(" 13: Palindrom");
                Console.WriteLine(" 14: ListSortedValues");
                Console.WriteLine(" 15: CapICuaValues");
                Console.WriteLine(" 16: ListSameValues");
                Console.WriteLine(" 17: ListSumValues");
                Console.WriteLine(" 18: IvaPrices");
                Console.WriteLine(" 19: CovidGrowRate");
                Console.WriteLine(" 20: BicicleDistance");
                Console.WriteLine(" 21: ValueNearAvg");
                Console.WriteLine(" 22: ISBN");
                Console.WriteLine(" 23: ISBN13");
                Console.WriteLine(" -1: Salir del menu \n");
                Console.WriteLine("Cuando un ejercicio acabe, presiona cualquier tecla para volver al menu.\n");
                Console.Write("Introduce el número del ejercicio que quieras ejecutar: ");
                n = Convert.ToInt32(Console.ReadLine());
                
                switch (n)
                {
                    case -1:
                        break;
                    case 1:
                        DayOfTheWeek();
                        break;
                    case 2:
                        PlayerNumbers();
                        break;
                    case 3:
                        CandidatesList();
                        break;
                    case 4:
                        LetterInword();
                        break;
                    case 5:
                        AddValuesToList();
                        break;
                    case 6:
                        Swap();
                        break;
                    case 7:
                        PushButtonPadlockSimulator();
                        break;
                    case 8:
                        BoxesOpenedCounter();
                        break;
                    case 9:
                        MinOf10Values();
                        break;
                    case 10:
                        IsThereAMultipleOf7();
                        break;
                    case 11:
                        SearchInOrdered();
                        break;
                    case 12:
                        InverseOrder();
                        break;
                    case 13:
                        Palindrom();
                        break;
                    case 14:
                        ListSortedValues();
                        break;
                    case 15:
                        CapICuaValues();
                        break;
                    case 16:
                        ListSameValues();
                        break;
                    case 17:
                        ListSumValues();
                        break;
                    case 18:
                        IvaPrices();
                        break;
                    case 19:
                        CovidGrowRate();
                        break;
                    case 20:
                        BicicleDistance();
                        break;
                    case 21:
                        ValueNearAvg();
                        break;
                    case 22:
                        Isbn();
                        break;
                    case 23:
                        Isbn13();
                        break;
                }
                Console.Clear();
            } while (n != -1);
        }
        static void DayOfTheWeek()  // Description: Introduce un número entero del 0 al 6 para saber a que día de la semana corresponde.
        {
            string[] dia = {"Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"};
            Console.Write("Introduce un número entero: ");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(dia[n]);
            Console.ReadKey();
            Console.Clear();
        }

        static void PlayerNumbers() // Description: Introduce 5 números que corresponden a 5 jugadores.
        {
            int[] a = new int[5];
            Console.WriteLine("Introduce los 5 jugadores que jugarán el partido: ");
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }
            
            Console.Write("Alineación de jugadores: [");
            for (int i = 0; i < a.Length; i++)
            {
                if ( i != a.Length-1) Console.Write("{0}, ",a[i]);
                else Console.Write(a[i]);
            }
            Console.Write("]");
        }

        static void CandidatesList()
            /*
            *  Description: Introduce el número de candidatos, después introduce los nombres de los candidatos,
            *  por ultimo puedes consultar cada candidato escribiendo su número correspondiente. Introduce -1 para salir del programa.
            */
        {
            Console.Write("Introduce el número de candidatos: ");
            int a = Convert.ToInt32(Console.ReadLine());
            string[] b = new String[a];

            for (int i = 0; i < b.Length; i++) //añadir elementos al string
            {
                b[i] = Convert.ToString(Console.ReadLine());
            }

            int c = 1;
            while (c != -1) // buscar elementos de un string
            {
                Console.WriteLine(b[c - 1]);
                c = Convert.ToInt32(Console.ReadLine());
            }
        }

        static void LetterInword() // Description: Escribe una palabra y la posición de una letra. 
        {
            Console.Write("Escribe una palabra: ");
            string a = Convert.ToString(Console.ReadLine());
            Console.Write("Posición de la letra: ");
            int b = Convert.ToInt32(Console.ReadLine());
            if (a != null) Console.WriteLine(a[b]);
        }

        static void AddValuesToList() // Description: Vector de 50 decimales y modifica el valor de posiciones específicas.
        {
            float[] a = new float[50];
            a[0] = 31.0f;
            a[1] = 56.0F;
            a[19] = 12.0F;
            a[49] = 79.0F;

            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("Posición {0}: {1}", i, a[i]);
            }
        }

        static void Swap() // Description: Introduce 4 números, el programa intercambiará el primero por el último.
        {
            Console.WriteLine("Escribe 4 números: ");
            int[] a = new int[4];

            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

            (a[0], a[3]) = (a[3], a[0]);

            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("Posición {0}: {1}", i, a[i]);
            }
        }

        static void PushButtonPadlockSimulator()  // Description: Introduce la posición del botón que quieres modificar. Introduce -1 para finalizar el programa.
        {
            bool[] a = new bool[8];
            int b = Convert.ToInt32(Console.ReadLine());

            while (b != -1)
            {
                a[b] = !a[b];
                b = Convert.ToInt32(Console.ReadLine());
            }

            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("Botón {0}: {1}", i, a[i]);
            }
        }

        static void BoxesOpenedCounter()  // Description: Introduce la posición del botón que quieres modificar. Introduce -1 para finalizar el programa.
        {
            int[] a = new int[10];

            int b = Convert.ToInt32(Console.ReadLine());
            while (b != -1)
            {
                a[b]++;
                b = Convert.ToInt32(Console.ReadLine());
            }

            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("Han accedido {1} veces a la caja número {0}", i, a[i]);
            }
        }

        static void MinOf10Values() // Description: Introduce la posición del botón que quieres modificar. Introduce -1 para finalizar el programa.
        {
            int[] a = new int[10];

            Console.WriteLine("Introduce 10 números:");
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

            int b = a[0];
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] < b)
                {
                    b = a[i];
                }
            }

            Console.WriteLine("El valor más pequeño de la lista es {0}", b);
        }

        static void IsThereAMultipleOf7()  // Description: Muestra los números de la lista divisibles entre 7.
        {
            int[] a =
            {
                4, 8, 9, 40, 54, 84, 40, 6, 84, 1, 1, 68, 84, 68, 4, 840, 684, 25, 40, 98, 54, 687, 31, 4894, 468, 46,
                84687, 894, 40, 846, 1681, 618, 161, 846, 84687, 6, 848
            };

            Console.Write("Números de la lista divisibles entre 7: ");
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] % 7 == 0)
                {
                    Console.Write("{0} ", a[i]);
                }
            }
        }

        static void SearchInOrdered()  // Description: Crea una lista de 10 valores y comprueba si un valor está dentro de la lista.
        {
            int[] a = new int[10];

            Console.WriteLine("Introduce 10 números en la lista:");

            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.Write("¿Qué valor quieres comprobar? ");
            int b = Convert.ToInt32(Console.ReadLine());

            if (((IList) a).Contains(b))
            {
                Console.WriteLine(true);
            }
            else
            {
                Console.WriteLine(false);
            }
        }

        static void InverseOrder()  // Description: Introduce 10 números, el programa intercambiará su orden.
        {
            Console.WriteLine("Introduce 10 números: ");
            int[] a = new int[10];

            for (int i = a.Length - 1; i >= 0; i--)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("Posición {0}: {1}", i, a[i]);
            }
        }

        static void Palindrom() // Description: Escribe una palabra para saber si es Palindrom.
        {
            Console.Write("Escribe una palabra para saber si es Palindrom: ");
            string a = Convert.ToString(Console.ReadLine());
            if (a != null)
            {
                string firstHalf = a.Substring(0, a.Length / 2);
                char[] arr = a.ToCharArray();

                Array.Reverse(arr);

                string temp = new string(arr);
                string secondHalf = temp.Substring(0, temp.Length / 2);

                if (firstHalf == secondHalf) Console.WriteLine ("Es Palindrom!");
                else Console.WriteLine("No es Palindrom");
            }
        }
        
        static void ListSortedValues() // Description: Identifíca si una lista de números es Cap I Cua.
        {
            Console.Write("Introduce la longitud de la lista: "); 
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce los valores de la lista: ");
            int[] b = new int[a];
            for (int i = 0; i < b.Length; i++)
            {
                b[i] = Convert.ToInt32(Console.ReadLine());
            }

            int c = b[0];
            for (int i = 0; i < b.Length; i++) //comprobar los valores
            {
                if (b[i] < c) // Si están desordenados se acaba el bucle
                {
                    break;
                }

                c = b[i];
            }

            if (b[a - 1] == c) Console.WriteLine("Ordenados");
            else Console.WriteLine("Desordenados");
        }

        static void CapICuaValues() // Description: Identifíca si una lista de números es Cap I Cua.
        {
            Console.Write("Introduce la longitud de la lista: "); 
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce los valores de la lista: ");
            int[] b = new int[a];
            int[] c = new int[a];

            for (int i = 0; i < b.Length; i++)
            {
                b[i] = Convert.ToInt32(Console.ReadLine());
            }

            for (int i = b.Length, m = 0; i > 0; i--, m++) // Lista inversa
            {
                c[m] = b[i - 1];
            }

            if (b.SequenceEqual(c)) Console.WriteLine("CAP I CUA");
        }

        static void ListSameValues() // Description: Introduce una lista de valores para comprobar si son iguales.
        {
            Console.Write("Introduce la longitud de la lista A: "); 
            int x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce los valores de la lista A: ");
            int[] a = new int[x];
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }
            
            Console.Write("Introduce la longitud de la lista B: "); 
            int y = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce los valores de la lista B: ");
            int[] b = new int[y];
            for (int i = 0; i < b.Length; i++)
            {
                b[i] = Convert.ToInt32(Console.ReadLine());
            }

            if (a.SequenceEqual(b)) Console.WriteLine("Son iguales");
            else Console.WriteLine("Son distintos");

        }

        static void ListSumValues() // Description: Introduce una lista de valores para sumarlos.
        {
            Console.Write("Introduce la longitud de la lista: ");
            int l = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce los valores de la lista: ");
            int[] a = new int[l];
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

            int result = 0;
            foreach (var n in a)
            {
                result += n;
            }
            Console.WriteLine("La suma de la lista es: {0}",result);
        }

        static void IvaPrices() // Description: Calculo de precios más IVA.
        {
            Console.WriteLine("Introduce los valores de la lista: ");
            int[] a = new int[10];
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }
            
            foreach (int n in a)
            {
                double result = n + n*0.21;
                Console.WriteLine("Precio original: {0} | Precio más IVA: {1}",n,result);
            }
        }

        static void CovidGrowRate() // Description: Calculo de aumento de infección semanal.
        {
            Console.Write("Introduce el número de semanas de infección: "); 
            int l = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce el número de casos por semana:");
            double[] a = new double[l];
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write("{0}º Semana: ",i+1);
                a[i] = Convert.ToDouble(Console.ReadLine());
            }
            
            for (int i = 0; i < a.Length-1; i++)
            {
                double result = a[i+1] / a[i]; 
                Console.WriteLine("Porcentaje de aumento de infección ({1}º Semana): {0}",result,i+1);
            }
        }

        static void BicicleDistance() // Description: Introduce la velocidad para saber la distancia recorrida al cabo de 10 segundos.
        {
            Console.Write("Introduce la velocidad en m/s: ");
            double v = Convert.ToDouble(Console.ReadLine());
            double[] a = new double[10];
            Console.WriteLine("Distancia recorrida:");
            for (int i = 0; i < a.Length; i++)  // Nota: Los valores decimáles se esciben con comas "," no con puntos "."
            {
                a[i] = (i+1) * v;
                Console.WriteLine("Segundo {0}: {1} metros",i+1,a[i]);
            }
        }

        static void ValueNearAvg() // Description: Introduce una lista de números, el programa identificará el más cercano a la media de la lista.
        {
            Console.Write("Introduce la longitud de la lista: ");
            int l = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce los valores de la lista:");
            int[] a = new int[l];
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

            double m = 0;
            foreach (var n in a)
            {
                m += n;
            }
            m /= 4;
            
            int num1 = 0;
            int num2 = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] <= m)
                {
                    num1 = a[i];
                    num2 = a[i+1];
                }
            }
            Console.Write("Valor más próximo a la media: ");
            if ( m - num1 < num2 - m) Console.WriteLine(num1);
            else Console.WriteLine(num2);
        }

        static void Isbn() // Description: Introduce un ISBN para saber si es válido
        {
            int[] a = new int[10];
            Console.WriteLine("Introduce los números del ISBN:");
            int result = 0;
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
                if ( i != 9) result += a[i] * (i + 1);
            }
            result %= 11;
            bool r = result == a[9];
            Console.WriteLine("ISBN válido: {0}",r);
        }
        
        static void Isbn13() // Description: Introduce un ISBN13 para saber si es válido
        {
            int[] a = new int[13];
            Console.WriteLine("Introduce los números del ISBN:");
            int result = 0;
            bool s = true;
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
                if (i != 12)
                {
                    if (s)
                    {
                        result += a[i] * 1;
                        s = false;
                    }
                    else
                    {
                        result += a[i] * 3;
                        s = true;
                    }
                }
            }
            result += a[12];
            bool r = result%10 == 0;
            Console.WriteLine("ISBN válido: {0}",r);
        }
    }
}
